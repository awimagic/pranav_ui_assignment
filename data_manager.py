import yaml
import random
from collections import namedtuple
import logging


logging.basicConfig(level=logging.DEBUG,
                    format='%(lineno)-3d - %(message)s')

_Result = namedtuple('Result', ['success', 'value', 'error'])


def findModel(kb, modelname):
    logging.debug('modelname: %s', modelname)
    return next(m for m in kb['models'] if m['model'] == modelname)

class DataManager(object):
    def __init__(self):
        self.configfile = 'KB.yaml'
        with open(self.configfile, 'r') as f:
            self.config = yaml.load(f)

    def get_kb_names(self):
        '''return iterable of all KBs that are being managed.
        a possible reply is ['fintech', 'unix', 'command']

        '''
        return list(self.config['KBs'].keys())

    def get_top_modelname(self, kbname):
        with open(self.config['KBs'][kbname]['path']) as f:
            kb = yaml.load(f)
            return kb['topmodel']

    def get_current_state(self, kbname=None, modelname=None, path=None):
        #return {kbname: {'value': None, 'domain': ['foo', 'bar'], 'id': 'id'}}
        with open(self.config['KBs'][kbname]['path']) as f:
            kb = yaml.load(f)
            model = findModel(kb, modelname)
            if path:
                path.append(model['model'])
            else:
                path = [model['model']]
            data = {}
            for para in model['parameters']:
                para_value = para['value']
                child = None
                domain = []
                if para_value:
                    domain = [para_value]
                    if 'model' in para_value.strip(' ()').split():
                        child = self.get_current_state(
                            kbname,
                            modelname=para_value.strip(' ()').split()[1],
                            path=path)
                        logging.debug('child: %s', child)
                        path.pop()
                    else:
                        pass
                else:
                    domain = para['domain']
                id = ':'.join(path)
                data.setdefault(para['para'],
                                {'domain': domain, 'id': id, 'child': child})
            return data

    def submit(self, id=None, value=None):
        parts = value.strip(' ()').split()[::-1]  # reversing so that
                                                  # parts[0] is always the value
        if random.random() < 0.1:
            return _Result(success=False, error='some error', value=None)
        else:
            return _Result(success=True, value='good', error=None)
