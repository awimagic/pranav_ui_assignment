$(document).ready(function() {
    console.log('ready!!!')

    $('#kb').change(function() {
        console.log('kb selection performed');
        $.getJSON('/kb', {kbname: $(this).val()}, function(resp) {
            $('#topModel').html(resp.html);
        });
    });

});


$(document).on('change', '.select_para', function() {
    console.log(this);
    var child = '#' + CSS.escape($(this).attr('id')) + '-child';
    $.getJSON('/set_value', {id: $(this).attr('id'), value: $(this).val()},
              function(resp, status) {
                  if (resp.error) {
                      console.log('ERROR');
                      //$(child).html(resp.html).addClass('bg-danger');
                      $(child).html(resp.html).addClass('alert alert-danger');
                  } else {
                      $('#topModel').html(resp.html);
                  }
              });
});


$(document).on('keypress', '.input_para', function(e) {
    if(e.which == 13) {
        console.log(this);
        console.log($(this).val());
        $(this).fadeOut('fast').fadeIn('fast').blur();
        var child = '#' + CSS.escape($(this).attr('id')) + '-child';
        $.getJSON('/set_value', {id: $(this).attr('id'), value: $(this).val()},
                  function(resp, status) {
                      if (resp.error) {
                          console.log('ERROR');
                          $(child).html(resp.html).addClass('alert alert-danger');
                      } else {
                          $('#topModel').html(resp.html);
                      }
                  });
    }
});
