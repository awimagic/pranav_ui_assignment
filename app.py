from flask import (Flask, jsonify, render_template, request, session,
                   redirect, url_for)
import logging
from colorama import Fore
from uuid import uuid4
from data_manager import DataManager


def cprint(msg):
    print('{}{}{}'.format(Fore.RED, msg, Fore.RESET))


def value_of(x):
    components = x.strip(' ()').split()
    if len(components) == 1:
        r = {'kind': None, 'value': components[0]}
        logger.debug(r)
        return r
    elif len(components) == 2:
        r = {'kind': components[0], 'value': components[1]}
        logger.debug(r)
        return r
    else:
        raise Exception('Could not parse value')


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

#formatter = logging.Formatter('%(asctime)s - %(name)-8s - %(levelname)-8s - %(pathname)s:%(funcName)s():%(lineno)d\n%(message)s')

formatter = logging.Formatter('%(message)s --- %(pathname)s:%(funcName)s():%(lineno)d - %(asctime)s - %(name)s - %(levelname)s')

# file handler
fh = logging.FileHandler('app.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

# console handler
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
logger.addHandler(ch)


app = Flask(__name__)
app.secret_key = 'pbot1234'

DATA = {}


def get(key):
    return DATA[session['sid']][key]


@app.route('/reset')
def reset():
    logger.info('ending session id %s', session['sid'])
    DATA.pop(session['sid'], None)
    session.pop('sid', None)
    return redirect(url_for('index'))


@app.route('/kb')
def kb():
    '''select KB and load its parameters'''
    logger.info(session['sid'])
    kbname = request.args['kbname']
    DATA[session['sid']].setdefault('kbname', kbname)
    logger.info('kb: %s selected', kbname)
    dm = get('dm')
    topmodel = dm.get_top_modelname(kbname)
    parameters = dm.get_current_state(kbname, topmodel)
    html = render_template('model.html', params=parameters,
                           id=dm.get_top_modelname(kbname),
                           title=dm.get_top_modelname(kbname))
    error = False
    return jsonify(html=html, error=error)


@app.route('/set_value')
def set_value():
    id = request.args['id']
    value = request.args['value']
    logger.debug('id: %s, value: %s', id, value)
    dm = get('dm')
    kbname = get('kbname')
    result = dm.submit(id=id, value=value)
    if result.success:
        topmodel = dm.get_top_modelname(kbname)
        parameters = dm.get_current_state(kbname, topmodel)
        html = render_template('model.html', params=parameters,
                               id=dm.get_top_modelname(),
                               title=dm.get_top_modelname())
        error = False
    else:
        html = 'ERROR: {}'.format(result.error)
        error = True
    return jsonify(html=html, error=error)


@app.route('/')
def index():
    session['sid'] = str(uuid4())
    DATA.setdefault(session['sid'], {})
    logger.info('session id: %s started', session['sid'])
    dm = DataManager()
    DATA[session['sid']].setdefault('dm', dm)
    return render_template('index.html', kbs=dm.get_kb_names())


if __name__ == '__main__':
    app.run(debug=True)
